#ifndef REGISTERTABLE_H
#define REGISTERTABLE_H
#include <inttypes.h>
#include <stdio.h>

/**
 * A Register object containing data related to a MIPS register
 */
struct _Register {

    char* Name;
    uint8_t Number;
    char* Bits;
};

typedef struct _Register Register;

/**
 * Returns a Register with the given name, if it exists
 */
Register* getRegisterWithName(char* name);

#endif