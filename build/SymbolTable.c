#include <inttypes.h> // I used functions from these Standard modules in
#include <string.h>   // my solution; you may or may not need all of them,
#include <stdlib.h>   // and you might need additional ones, depending on
#include <stdio.h>    // your approach to the problem.
#include <assert.h>

#include "SymbolTable.h"

/**
 *  Returns a newly initializes table of Symbols
 */
SymbolTable* newSymbolTable() {

    SymbolTable* table = calloc(1, sizeof(SymbolTable));
    table->Size = 0;
    table->Capacity = 1;
    table->Table = calloc(table->Capacity, sizeof(Symbol*));

    return table;
}

/**
 * Adds a Symbol to a SymbolTable
 */
void addSymbol(SymbolTable* table, Symbol* symbol) {
    
    if (table->Size == table->Capacity)
    {
        //Increase table size
        table->Table = realloc(table->Table, table->Capacity * 2 * sizeof(Symbol*));
        if (!table->Table)
        {

            printf("%s", "CATASTROPHIC FAILURE: COULD NOT REALLOC TABLE");
            return;
        }

        // Check for successful allocation
        table->Capacity *= 2;
    }
    
    // Add new Symbol to SymbolTable
    
    table->Table[table->Size] = symbol;
    table->Size++;
}

/**
 * Frees dynamically allocated memory pertaining to a SymbolTable and its Symbols
 */
void freeSymbolTable(SymbolTable* table) {

    for (int i = 0; i < table->Size; i++) {
        freeSymbol(table->Table[i]);
        
    }

    if (table->Table) {
        free(table->Table);
    }
    free(table);
}

/**
 * Frees dynamically allocated memory pertaining to a Symbol and its fields
 */
void freeSymbol(Symbol *symbol) {
    if(symbol->Name) {
        free(symbol->Name);
    }

    if (symbol->BinValue) {
        free(symbol->BinValue);
    }
    free(symbol);
}

/**
 * Returns a newly initialized Symbol
 */
Symbol* initSymbol() {
    Symbol *symbol = calloc(1, sizeof(Symbol));
    symbol->Name = NULL;
    symbol->BinValue = NULL;

    return symbol;
}

/**
 * Returns a Symbol with a given name, if it exists
 */
Symbol *findSymbolWithName(SymbolTable *table, char *name) {

    for (int i = 0; i < table->Size; i++) {
        if (strcmp(table->Table[i]->Name, name) == 0) {
            return table->Table[i];
        } 
    }

    return NULL;
}
