#ifndef MNEMONICTABLE_H
#define MNEMONICTABLE_H

struct _Instruction {

    char* Mnemonic;
    char* Type;
    char* Opcode;
    char* Funct;
};

typedef struct _Instruction Instruction;



Instruction* getInstructionWithMnemonic(char* mnemonic);

#endif