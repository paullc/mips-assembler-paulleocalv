#ifndef ASSEMBLER_H
#define ASSEMBLER_H

#include <string.h> // my solution; you may or may not need all of them,
#include <stdlib.h> // and you might need additional ones, depending on
#include <stdio.h>  // your approach to the problem.
#include <assert.h>
#include <stdbool.h>
#include "ASMParser.h"
#include "ParseResult.h"
#include "ParsingHelp.h"
#include "MnemonicTable.h"
#include "SymbolTable.h"

/**
 * Entry point for the MIPS assembler
 */
int main(int argc, char **argv);

/**
 * Populates a symbol table with all symbols in the readFile
 */
void populateSymbols(char *line, FILE *readFile, SymbolTable *symbolTable, int *dataAddress, int *textAddress);

/**
 * Prints the data segment of a SymbolTable
 */
void printDataSegment(FILE *writeFile, SymbolTable *symbolTable);

/**
 * Prints all symbols in a SymbolTable
 */
void printSymbols(SymbolTable *symbolTable, FILE *writeFile);

/**
 * Sanitizes an input string by trimming undesired characters
 */
char *getSanitizedLine(char* sanitizedLine, char *line);

#endif