#include <inttypes.h> // I used functions from these Standard modules in
#include <string.h>   // my solution; you may or may not need all of them,
#include <stdlib.h>   // and you might need additional ones, depending on
#include <stdio.h>    // your approach to the problem.
#include <assert.h>

#include "MnemonicTable.h"


#define NUMINSTRUCTIONS 28

static Instruction MnemonicTable[NUMINSTRUCTIONS] = {
    {"add", "R", "000000", "100000"},
    {"addi", "I", "001000", NULL},
    {"addu", "R", "000000", "100001"},
    {"addiu", "I", "001001", NULL},
    {"and", "R", "000000", "100100"},
    {"andi", "I", "001100", NULL},
    {"beq", "I", "000100", NULL},
    {"blez", "I", "000110", NULL},
    {"bgtz", "I", "000111", NULL},
    {"bne", "I", "000101", NULL},
    {"j", "J", "000010", NULL},
    {"lui", "I", "001111", NULL},
    {"lw", "I", "100011", NULL},
    {"mul", "R", "011100", "000010"},
    {"nop", "R", NULL, NULL}, // Special
    {"nor", "R", "000000", "100111"},
    {"sll", "R", "000000", "000000"},
    {"slt", "R", "000000", "101010"},
    {"slti", "I", "001010", NULL},
    {"sra", "R", "000000", "000011"},
    {"srav", "R", "000000", "000111"},
    {"sub", "R", "000000", "100010"},
    {"sw", "I", "101011", NULL},
    {"syscall", "R", "000000", "001100"},
    {"move", "R", NULL, NULL}, // Special
    {"blt", "I", NULL, NULL},  // Special
    {"la", "I", NULL, NULL},   // Special
    {"li", "I", NULL, NULL}    // Special
};

Instruction* getInstructionWithMnemonic(char* mnemonic) {

    for(int i = 0; i < NUMINSTRUCTIONS; i++) {
        if (strcmp(mnemonic, MnemonicTable[i].Mnemonic) == 0) {
            return &MnemonicTable[i];
        }
    }

    return NULL;
}