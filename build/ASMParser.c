#include <inttypes.h> // I used functions from these Standard modules in
#include <string.h>   // my solution; you may or may not need all of them,
#include <stdlib.h>   // and you might need additional ones, depending on
#include <stdio.h>    // your approach to the problem.
#include <assert.h>

#include "ASMParser.h"
#include "ParseResult.h"
#include "MnemonicTable.h"
#include "RegisterTable.h"
#include "ParsingHelp.h"
#include "SymbolTable.h"

/**  Breaks up given MIPS32 assembly instruction and creates a proper 
 *   ParseResult object storing information about that instruction.
 * 
 *   Pre:  pASM points to an array holding the bits (as chars) of a
 *         syntactically valid assembly instruction, whose mnemonic is
 *         one of the following:
 *             add  addi  and  andi  lui  lw  or  ori  sub
 * 
 *   Returns:
 *         A pointer to a proper ParseResult object whose fields have been
 *         correctly initialized to correspond to the target of pASM.
 */
int parseASM(const char *const pASM, SymbolTable *symbolTable, FILE *writeFile, int *instrAddress)
{

   // Implement this function!

   //Parse the string to find the mnemonic

   char *ASMCopy = calloc(strlen(pASM) + 1, sizeof(char));
   strcpy(ASMCopy, pASM);

   char *token = strtok(ASMCopy, " \t");
   Instruction *instruction = getInstructionWithMnemonic(token);

   //Return null if instruction not found in table
   if (!instruction)
   {
      return 1;
   }

   //Create a ParseResult struct.
   ParseResult *result = calloc(1, sizeof(ParseResult));
   initParseResult(result);

   result->ASMInstruction = calloc((strlen(pASM) + 1), sizeof(char));
   strcpy(result->ASMInstruction, pASM);
   result->Mnemonic = calloc((strlen(instruction->Mnemonic) + 1), sizeof(char));
   strcpy(result->Mnemonic, instruction->Mnemonic);

   // PARSE INSTRUCTION ACCORDING TO TYPE
   if (strcmp(instruction->Type, "R") == 0)
   {
      parseRType(pASM, result, instruction, symbolTable, writeFile, instrAddress);
   }
   else if (strcmp(instruction->Type, "I") == 0)
   {
      parseIType(pASM, result, instruction, symbolTable, writeFile, instrAddress);
   }
   else if (strcmp(instruction->Type, "J") == 0)
   {
      parseJType(pASM, result, instruction, symbolTable, writeFile, instrAddress);
   }

   free(ASMCopy);
   clearResult(result);
   free(result);
   return 0;
}

/**
 * Parses R Type instructions
 */
void parseRType(const char *const pASM, ParseResult *result, Instruction *instruction, SymbolTable *symbolTable, FILE *writeFile, int *instrAddress)
{

   if (strcmp(instruction->Mnemonic, "syscall") == 0)
   {
      result->Opcode = calloc(strlen(instruction->Opcode) + 1, sizeof(char));
      strcpy(result->Opcode, instruction->Opcode);

      result->Funct = calloc((strlen(instruction->Funct) + 1),  sizeof(char));
      strcpy(result->Funct, instruction->Funct);

      printRType(result, instruction, writeFile);
   }
   else if (strcmp(instruction->Mnemonic, "nop") == 0)
   {
      parseASM("sll $zero, $zero, 0", symbolTable, writeFile, instrAddress);
   }
   else if (strcmp(instruction->Mnemonic, "move") == 0)
   {
      char rdName[6];
      char rsName[6];

      sscanf(result->ASMInstruction, "%*s %[^,], %[^,]", rdName, rsName);

      //Generate asm instruction string
      char asmString[255];
      sprintf(asmString, "addu %s, $zero, %s", rdName, rsName);
      parseASM(asmString, symbolTable, writeFile, instrAddress); // Hand off to ADDIU parser recursively.
   }
   else if (strcmp(instruction->Mnemonic, "sra") == 0 || strcmp(instruction->Mnemonic, "sll") == 0)
   {
      result->Opcode = calloc((strlen(instruction->Opcode) + 1), sizeof(char));
      strcpy(result->Opcode, instruction->Opcode);

      result->Funct = calloc((strlen(instruction->Funct) + 1), sizeof(char));
      strcpy(result->Funct, instruction->Funct);

      // ALLOCATE MEMORY FOR NECESSARY FIELDS
      int shiftAmount;
      result->rtName = calloc(6,  sizeof(char));
      result->rdName = calloc(6, sizeof(char));

      result->RT = calloc(6, sizeof(char));
      result->RD = calloc(6, sizeof(char));
      //---------------------------------------
      if (sscanf(result->ASMInstruction, "%*s %[^,], %[^,], %d", result->rdName, result->rtName, &shiftAmount) == 3)
      {
         Register *regT = getRegisterWithName(result->rtName);
         Register *regD = getRegisterWithName(result->rdName);

         result->rt = regT->Number;
         strcpy(result->RT, regT->Bits);

         result->rd = regD->Number;
         strcpy(result->RD, regD->Bits);

         result->Imm = shiftAmount;

         //CONVERT IMMEDIATE TO TWO'S COMPLEMENT BIT REPRESENTATION

         char binImm[6];
         binImm[5] = '\0';

         decToBin(binImm, result->Imm, "shiftAmount");

         result->IMM = calloc(strlen(binImm) + 1, sizeof(char));
         strcpy(result->IMM, binImm);

         printRType(result, instruction, writeFile);
      }
   }
   else
   {

      result->Opcode = calloc((strlen(instruction->Opcode) + 1), sizeof(char));
      strcpy(result->Opcode, instruction->Opcode);

      result->Funct = calloc((strlen(instruction->Funct) + 1), sizeof(char));
      strcpy(result->Funct, instruction->Funct);

      // ALLOCATE MEMORY FOR NECESSARY FIELDS
      result->rsName = calloc(6, sizeof(char));
      result->rtName = calloc(6, sizeof(char));
      result->rdName = calloc(6, sizeof(char));

      result->RS = calloc(6, sizeof(char));
      result->RT = calloc(6, sizeof(char));
      result->RD = calloc(6, sizeof(char));
      //---------------------------------------
      sscanf(result->ASMInstruction, "%*s %[^,], %[^,], %[^,]", result->rdName, result->rsName, result->rtName);

      if (strcmp(instruction->Mnemonic, "srav") == 0) {
         sscanf(result->ASMInstruction, "%*s %[^,], %[^,], %[^,]", result->rdName, result->rtName, result->rsName);
      }

      Register *regS = getRegisterWithName(result->rsName);
      Register *regT = getRegisterWithName(result->rtName);
      Register *regD = getRegisterWithName(result->rdName);

      result->rs = regS->Number;
      strcpy(result->RS, regS->Bits);

      result->rt = regT->Number;
      strcpy(result->RT, regT->Bits);

      result->rd = regD->Number;
      strcpy(result->RD, regD->Bits);

      result->Imm = 0;
      result->IMM = NULL;

      printRType(result, instruction, writeFile);
   }
}

/**
 * Parses I Type instructions
 */
void parseIType(const char *const pASM, ParseResult *result, Instruction *instruction, SymbolTable *symbolTable, FILE *writeFile, int *instrAddress)
{

   if (strcmp(instruction->Mnemonic, "lui") == 0)
   {
      result->Opcode = calloc((strlen(instruction->Opcode) + 1), sizeof(char));
      strcpy(result->Opcode, instruction->Opcode);

      result->rs = 0;
      result->RS = calloc(6, sizeof(char));
      strcpy(result->RS, "00000");
      result->rtName = calloc(6, sizeof(char));
      result->RT = calloc(6, sizeof(char));

      int imm;
      sscanf(result->ASMInstruction, "%*s %[^,], %d", result->rtName, &imm);

      result->Imm = imm;

      Register *regT = getRegisterWithName(result->rtName);
      result->rt = regT->Number;
      strcpy(result->RT, regT->Bits);

      //CONVERT IMMEDIATE TO TWO'S COMPLEMENT BIT REPRESENTATION

      char binImm[17];
      binImm[16] = '\0';

      decToBin(binImm, result->Imm, "I");
      result->IMM = calloc((strlen(binImm) + 1), sizeof(char));
      strcpy(result->IMM, binImm);

      printIType(result, instruction, writeFile);
   }
   else if (strcmp(instruction->Mnemonic, "lw") == 0 || strcmp(instruction->Mnemonic, "sw") == 0)
   {

      result->Opcode = calloc((strlen(instruction->Opcode) + 1), sizeof(char));
      strcpy(result->Opcode, instruction->Opcode);

      result->rtName = calloc(6, sizeof(char));
      result->rsName = calloc(6, sizeof(char));

      result->RT = calloc(6, sizeof(char));
      result->RS = calloc(6, sizeof(char));

      int imm;
      char labelName[32];

      if (sscanf(result->ASMInstruction, "%*s %[^,], %d(%[^,()])", result->rtName, &imm, result->rsName) == 3)
      { // Using offset
         result->Imm = imm;

         Register *regT = getRegisterWithName(result->rtName);
         Register *regS = getRegisterWithName(result->rsName);

         result->rt = regT->Number;
         strcpy(result->RT, regT->Bits);

         result->rs = regS->Number;
         strcpy(result->RS, regS->Bits);

         //CONVERT IMMEDIATE TO TWO'S COMPLEMENT BIT REPRESENTATION

         char binImm[17];
         binImm[16] = '\0';

         decToBin(binImm, result->Imm, "I");
         result->IMM = calloc((strlen(binImm) + 1), sizeof(char));
         strcpy(result->IMM, binImm);

         printIType(result, instruction, writeFile);
      } 
      else if (sscanf(result->ASMInstruction, "%*s %[^,], %s", result->rtName, labelName) == 2) // Label pseudo instruction
      { // Using label

         Symbol *foundSymbol = findSymbolWithName(symbolTable, labelName);

         if (!foundSymbol)
         {
            return;
         }

         //Generate asm instruction string
         char asmString[255];
         sprintf(asmString, "lw %s, %d($zero)", result->rtName, foundSymbol->Address);
         parseASM(asmString, symbolTable, writeFile, instrAddress); // Hand off to ADDIU parser recursively.

      }
   }
   else if (strcmp(instruction->Mnemonic, "la") == 0)
   {

      int imm = 0;

      char labelName[32];
      char rtName[6];

      sscanf(result->ASMInstruction, "%*s %[^,], %s", rtName, labelName);

      Symbol *foundSymbol = findSymbolWithName(symbolTable, labelName);

      if (!foundSymbol)
      {
         return;
      }
      imm = foundSymbol->Address;

      //Generate asm instruction string
      char asmString[255];
      sprintf(asmString, "addi %s, $zero, %d", rtName, imm);
      parseASM(asmString, symbolTable, writeFile, instrAddress); // Hand off to ADDI parser recursively.
   }
   else if (strcmp(instruction->Mnemonic, "li") == 0)
   {

      int imm = 0;
      result->rtName = calloc(6, sizeof(char));

      sscanf(result->ASMInstruction, "%*s %[^,], %d", result->rtName, &imm);


      //Generate asm instruction string
      char asmString[255];
      sprintf(asmString, "addiu %s, $zero, %d", result->rtName, imm);
      parseASM(asmString, symbolTable, writeFile, instrAddress); // Hand off to ADDIU parser recursively.
   }
   else if (strcmp(instruction->Mnemonic, "blez") == 0 || strcmp(instruction->Mnemonic, "bgtz") == 0)
   {

      result->Opcode = calloc((strlen(instruction->Opcode) + 1), sizeof(char));
      strcpy(result->Opcode, instruction->Opcode);

      result->rsName = calloc(6, sizeof(char));
      result->RS = calloc(6, sizeof(char));

      int imm = 0;
      char labelName[32];

      // Scan without label
      if (sscanf(result->ASMInstruction, "%*s %[^,], %d", result->rsName, &imm) == 2)
      {

         Register *regS = getRegisterWithName(result->rsName);

         result->rs = regS->Number;
         strcpy(result->RS, regS->Bits);

         if (result->Mnemonic[0] == 'b')
         { // is a branch instruction
            int targetAddress = (*instrAddress + (imm * 4) + 4);
            imm = (targetAddress - *instrAddress - 4) / 4;
         }

         result->Imm = imm;

         //CONVERT IMMEDIATE TO TWO'S COMPLEMENT BIT REPRESENTATION

         char binImm[17];
         binImm[16] = '\0';

         decToBin(binImm, result->Imm, "I");
         result->IMM = calloc((strlen(binImm) + 1), sizeof(char));
         strcpy(result->IMM, binImm);
      }
      //Scan with label
      else if (sscanf(result->ASMInstruction, "%*s %[^,], %s", result->rsName, labelName) == 2)
      {

         Register *regS = getRegisterWithName(result->rsName);

         result->rs = regS->Number;
         strcpy(result->RS, regS->Bits);

         Symbol *foundSymbol = findSymbolWithName(symbolTable, labelName);

         if (!foundSymbol)
         {
            return;
         }

         result->Imm = (foundSymbol->Address - *instrAddress - 4) / 4;

         //CONVERT IMMEDIATE TO TWO'S COMPLEMENT BIT REPRESENTATION

         char binImm[17];
         binImm[16] = '\0';

         decToBin(binImm, result->Imm, "I");
         result->IMM = calloc((strlen(binImm) + 1), sizeof(char));
         strcpy(result->IMM, binImm);
      }

      printIType(result, instruction, writeFile);
   }
   else if (strcmp(instruction->Mnemonic, "blt") == 0) // Interpreted as (SLT -> BNE)
   {
      int imm = 0;

      char labelName[32];
      char rsName[6];
      char rtName[6];

      if (sscanf(result->ASMInstruction, "%*s %[^,], %[^,], %s", rsName, rtName, labelName) == 3) {
         Symbol *foundSymbol = findSymbolWithName(symbolTable, labelName);

         if (!foundSymbol)
         {
            return;
         }

         //Generate asm instruction string
         char asmStringA[255];
         sprintf(asmStringA, "slt $at, %s, %s", rsName, rtName);
         parseASM(asmStringA, symbolTable, writeFile, instrAddress); // Hand off to ADDI parser recursively.

         char asmStringB[255];
         sprintf(asmStringB, "bne $at, $zero, %s", labelName);
         *instrAddress += 4;
         parseASM(asmStringB, symbolTable, writeFile, instrAddress); // Hand off to ADDI parser recursively.
      }
      else if (sscanf(result->ASMInstruction, "%*s %[^,], %[^,], %d", rsName, rtName, &imm) == 3)
      {

         //Generate asm instruction string
         char asmStringA[255];
         sprintf(asmStringA, "slt $at, %s, %s", rsName, rtName);
         parseASM(asmStringA, symbolTable, writeFile, instrAddress); // Hand off to ADDI parser recursively.

         char asmStringB[255];
         sprintf(asmStringB, "bne $at, $zero, %d", imm);
         *instrAddress += 4;
         parseASM(asmStringB, symbolTable, writeFile, instrAddress); // Hand off to ADDI parser recursively.
      }
   }
   else
   {

      result->Opcode = calloc((strlen(instruction->Opcode) + 1), sizeof(char));
      strcpy(result->Opcode, instruction->Opcode);

      result->rtName = calloc(6, sizeof(char));
      result->rsName = calloc(6, sizeof(char));

      result->RT = calloc(6, sizeof(char));
      result->RS = calloc(6, sizeof(char));

      int imm = 0;
      char labelName[32];

      // Scan without label
      if (sscanf(result->ASMInstruction, "%*s %[^,], %[^,], %d", result->rtName, result->rsName, &imm) == 3)
      {

         if (instruction->Mnemonic[0] == 'b')
         {
            sscanf(result->ASMInstruction, "%*s %[^,], %[^,], %d", result->rsName, result->rtName, &imm); // reverse rt and rs
         }

         Register *regT = getRegisterWithName(result->rtName);
         Register *regS = getRegisterWithName(result->rsName);

         result->rt = regT->Number;
         strcpy(result->RT, regT->Bits);

         result->rs = regS->Number;
         strcpy(result->RS, regS->Bits);

         if (result->Mnemonic[0] == 'b')
         { // is a branch instruction
            int targetAddress = (*instrAddress + (imm * 4) + 4);
            imm = (targetAddress - *instrAddress - 4) / 4;
         }

         result->Imm = imm;

         //CONVERT IMMEDIATE TO TWO'S COMPLEMENT BIT REPRESENTATION

         char binImm[17];
         binImm[16] = '\0';

         decToBin(binImm, result->Imm, "I");
         result->IMM = calloc((strlen(binImm) + 1), sizeof(char));
         strcpy(result->IMM, binImm);
      }
      //Scan with label
      else if (sscanf(result->ASMInstruction, "%*s %[^,], %[^,], %s", result->rtName, result->rsName, labelName) == 3)
      {

         if (instruction->Mnemonic[0] == 'b')
         {
            sscanf(result->ASMInstruction, "%*s %[^,], %[^,], %s", result->rsName, result->rtName, labelName); // reverse rt and rs
         }

         Register *regT = getRegisterWithName(result->rtName);
         Register *regS = getRegisterWithName(result->rsName);

         result->rt = regT->Number;
         strcpy(result->RT, regT->Bits);

         result->rs = regS->Number;
         strcpy(result->RS, regS->Bits);

         Symbol *foundSymbol = findSymbolWithName(symbolTable, labelName);
         imm = foundSymbol->Address;

         if (!foundSymbol)
         {
            return;
         }

         if (result->Mnemonic[0] == 'b')
         { // is a branch instruction
            imm = (foundSymbol->Address - *instrAddress - 4) / 4;
         }
         result->Imm = imm;

         //CONVERT IMMEDIATE TO TWO'S COMPLEMENT BIT REPRESENTATION

         char binImm[17];
         binImm[16] = '\0';

         decToBin(binImm, result->Imm, "I");
         result->IMM = calloc((strlen(binImm) + 1), sizeof(char));
         strcpy(result->IMM, binImm);
      }

      printIType(result, instruction, writeFile);
   }
}

/**
 * Parses J Type instructions
 */
void parseJType(const char *const pASM, ParseResult *result, Instruction *instruction, SymbolTable *symbolTable, FILE *writeFile, int *instrAddress)
{
   int imm = 0;
   char labelName[32];

   result->Opcode = calloc((strlen(instruction->Opcode) + 1), sizeof(char));
   strcpy(result->Opcode, instruction->Opcode);

   if (sscanf(result->ASMInstruction, "%*s %[^,]", labelName) == 1)
   {

      Symbol *foundSymbol = findSymbolWithName(symbolTable, labelName);

      if (!foundSymbol)
      {
         return;
      }

      imm = (foundSymbol->Address - 0) / 4;
      result->Imm = imm;

      //CONVERT IMMEDIATE TO TWO'S COMPLEMENT BIT REPRESENTATION

      char binImm[27];
      binImm[26] = '\0';

      decToBin(binImm, result->Imm, "J");
      result->IMM = calloc((strlen(binImm) + 1), sizeof(char));
      strcpy(result->IMM, binImm);
   }

   printJType(result, instruction, writeFile);
}

/**
 * Prints R Type instructions
 */
void printRType(ParseResult *result, Instruction *instruction, FILE *writeFile)
{
   if (strcmp(instruction->Mnemonic, "syscall") == 0)
   {
      fprintf(writeFile, "%s%s%s\n",
              result->Opcode ? result->Opcode : "",
              "00000000000000000000",
              result->Funct ? result->Funct : "");
   }
   else
   {
      fprintf(writeFile, "%s%s%s%s%s%s\n",
              result->Opcode ? result->Opcode : "",
              result->RS ? result->RS : "00000",
              result->RT ? result->RT : "00000",
              result->RD ? result->RD : "00000",
              result->IMM ? result->IMM : "00000",
              result->Funct ? result->Funct : "");
   }
}

/**
 * Prints I Type instructions
 */
void printIType(ParseResult *result, Instruction *instruction, FILE *writeFile)
{
   if (instruction->Mnemonic[0] == 'b')
   {
      fprintf(writeFile, "%s%s%s%s\n",
              result->Opcode ? result->Opcode : "",
              result->RS ? result->RS : "00000",
              result->RT ? result->RT : "00000",
              result->IMM ? result->IMM : "");
   }
   else
   {
      fprintf(writeFile, "%s%s%s%s\n",
              result->Opcode ? result->Opcode : "",
              result->RS ? result->RS : "00000",
              result->RT ? result->RT : "00000",
              result->IMM ? result->IMM : "");
   }
}

/**
 * Prints J Type instructions
 */
void printJType(ParseResult *result, Instruction *instruction, FILE *writeFile)
{
   fprintf(writeFile, "%s%s\n",
           result->Opcode ? result->Opcode : "",
           result->IMM ? result->IMM : "");

}

/**
 *  Converts an integer to its binary representation string.
 */
void decToBin(char* binImm, int imm, char *type)
{
   if (strcmp(type, "I") == 0)
   { // 16 bits
      int pos = 0;
      uint16_t mask = 0b1000000000000000;

      while (mask > 0)
      {
         binImm[pos] = (imm & mask) == 0 ? '0' : '1';
         mask = mask >> 1;
         pos++;
      }
   }
   else if (strcmp(type, "J") == 0) // 26 bits
   {
      int pos = 0;
      uint32_t mask = 0b10000000000000000000000000;

      while (mask > 0)
      {
         binImm[pos] = (imm & mask) == 0 ? '0' : '1';
         mask = mask >> 1;
         pos++;
      }
   }
   else if (strcmp(type, "label") == 0) // 32 bits
   {
      int pos = 0;
      uint32_t mask = 0b10000000000000000000000000000000;

      while (mask > 0)
      {
         binImm[pos] = (imm & mask) == 0 ? '0' : '1';
         mask = mask >> 1;
         pos++;
      }
   }
   else if (strcmp(type, "shiftAmount") == 0) // 32 bits
   {
      int pos = 0;
      uint8_t mask = 0b10000;

      while (mask > 0)
      {
         binImm[pos] = (imm & mask) == 0 ? '0' : '1';
         mask = mask >> 1;
         pos++;
      }
   }
}

/**
 * Converts an character to its binary representation string.
 */
void charToBin(char *localBinary, char character)
{

   int pos = 0;
   uint8_t mask = 0b10000000;
   int value = (int) character;

   while (mask > 0)
   {
      localBinary[pos] = (value & mask) == 0 ? '0' : '1';
      mask = mask >> 1;
      pos++;
   }
}
/**
 * Trims leading and trailing whitespace from a string
 */
char *trimLeadingTrailingWhitespace(char *sanitizedLine, char *str) {

   sscanf(str, " %[^\t\n]", sanitizedLine);

   return sanitizedLine;
}

/**
 * Initializes the fields of a ParseResult object
 */
void initParseResult(ParseResult *result)
{
   result->Funct = NULL;

   result->rsName = NULL;
   result->rtName = NULL;
   result->rdName = NULL;

   result->RS = NULL;
   result->RT = NULL;
   result->RD = NULL;

   result->rs = 255;
   result->rt = 255;
   result->rd = 255;

   result->Imm = 0;
   result->IMM = NULL;
}
