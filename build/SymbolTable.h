#ifndef SYMBOLTABLE_H
#define SYMBOLTABLE_H

/**
 *  Enumerates the potential symbol types
 */
enum _SymbolType {
    HOMO_ARRAY,
    HETERO_ARRAY,
    NO_ARRAY,
    ASCIIZ,
    TEXT_LABEL,
    NONE
};

typedef enum _SymbolType SymbolType;

/**
 *  A symbol struct containing information relating to a symbol in an ASM file
 */
struct _Symbol
{
    char *Name;
    int Address;
    char *BinValue;
    SymbolType Type;
};

typedef struct _Symbol Symbol;

/**
 *  A table of Symbols
 */
struct _SymbolTable
{
    Symbol **Table;
    int Size;
    int Capacity;
};

typedef struct _SymbolTable SymbolTable;

/**
 *  Returns a newly initializes table of Symbols
 */
SymbolTable *newSymbolTable();

/**
 * Adds a Symbol to a SymbolTable
 */
void addSymbol(SymbolTable *table, Symbol *symbol);

/**
 * Frees dynamically allocated memory pertaining to a SymbolTable and its Symbols
 */
void freeSymbolTable(SymbolTable *table);

/**
 * Frees dynamically allocated memory pertaining to a Symbol and its fields
 */
void freeSymbol(Symbol *symbol);

/**
 * Returns a newly initialized Symbol
 */
Symbol *initSymbol();

/**
 * Returns a Symbol with a given name, if it exists
 */
Symbol *findSymbolWithName(SymbolTable *table, char* name);

#endif