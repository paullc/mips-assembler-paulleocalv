#include "Assembler.h"

/**
 * Entry point for the MIPS assembler
 */
int main(int argc, char **argv)
{

    FILE *readFile = fopen(argv[1], "r");
    FILE *writeFile = fopen(argv[2], "w+");

    if (!readFile || !writeFile)
    {
        return 1;
    }

    // 32 Bit Addresses
    int dataAddress = 8192;
    int textAddress = 0;

    //Keep track of beginning of file for later
    long int fileStart = ftell(readFile);
    char line[256];

    //Generate SymbolTable
    SymbolTable *symbolTable = newSymbolTable();

    //Seach ASM for .data and populate the symbol table
    populateSymbols(line, readFile, symbolTable, &dataAddress, &textAddress);

    if (argc > 3 && strcmp(argv[3], "-symbols") == 0)
    { // -symbols flag
        //Print symbols only
        printSymbols(symbolTable, writeFile);
        return 0;
    }

    //Go back to start of Read File and then Parse ASM and output Binary
    textAddress = 0;
    fseek(readFile, fileStart, SEEK_SET);
    while (fgets(line, 256, readFile))
    {
        char sanitizedLine[256] = "";
        getSanitizedLine(sanitizedLine, line);

        //Skip if line is not an instruction
        if (strlen(sanitizedLine) == 0 || sanitizedLine[0] == '#' || strchr(sanitizedLine, ':') || strchr(sanitizedLine, '.'))
        {
            continue;
        }

        if (parseASM(sanitizedLine, symbolTable, writeFile, &textAddress) == 1)
        {
            return 1;
        }

        textAddress += 4;
    }

    printDataSegment(writeFile, symbolTable);

    freeSymbolTable(symbolTable);

    fclose(readFile);
    fclose(writeFile);

    return 0;
}

/**
 * Populates a symbol table with all symbols in the readFile
 */
void populateSymbols(char *line, FILE *readFile, SymbolTable *symbolTable, int *dataAddress, int *textAddress)
{

    bool inData = false;
    bool inText = false;
    int linePosition = 0;

    while (fgets(line, 256, readFile))
    {
        char sanitizedLine[256] = "";
        getSanitizedLine(sanitizedLine, line);
        sanitizedLine[strcspn(line, "\n")] = 0;
        //Skip if line is comment or empty

        char lineCopy[256];
        strcpy(lineCopy, sanitizedLine);
        char *token = strtok(lineCopy, " \t");

        if (strlen(sanitizedLine) == 0 || sanitizedLine[0] == '#')
        {
            continue;
        }
        else if (strcmp(sanitizedLine, ".data") == 0)
        {
            inData = true;
            inText = false;
            continue;
        }
        else if (strcmp(sanitizedLine, ".text") == 0)
        {
            inData = false;
            inText = true;
            continue;
        }

        //Get symbols from either .text or .data sections
        if (inData)
        { // IN DATA SECTION

            //GET INFO ABOUT CURRENT SYMBOL
            char *name = calloc(strlen(token), sizeof(char));
            strncpy(name, token, strlen(token) - 1);
            name[strlen(name)] = '\0'; //null terminate name

            //Get type
            token = strtok(NULL, " \t");
            char *type = calloc(strlen(token) + 1, sizeof(char));
            strcpy(type, token);

            if (strchr(sanitizedLine, ','))
            {
                token = strtok(NULL, ", \t");
            }
            else
            {
                token = strtok(NULL, " \t");
            }

            char *value;

            if (strcmp(type, ".asciiz") == 0)
            {
                value = calloc(strlen(sanitizedLine) + 1, sizeof(char));
                sscanf(sanitizedLine, "%*s %*s \"%[^\"]\"", value);
            }
            else
            {
                value = calloc(strlen(token) + 1, sizeof(char));
                strcpy(value, token);
            }

            //GET INFO ABOUT NEXT SYMBOL
            long int fileMarker = ftell(readFile);
            char nextLine[256];
            fgets(nextLine, 256, readFile);

            bool pad = strstr(nextLine, ".word") || !strchr(nextLine, ':') ? true : false;
            fseek(readFile, fileMarker, SEEK_SET);

            //Initialize Symbol
            Symbol *newSymbol = initSymbol();
            newSymbol->Name = calloc(strlen(name) + 1, sizeof(char));
            strcpy(newSymbol->Name, name);
            newSymbol->Address = *dataAddress;

            if (strcmp(type, ".asciiz") == 0)
            { // Parse ASCII type

                newSymbol->Type = ASCIIZ;

                // Calculate number of lines needed and amount of padding needed
                int numChars = strlen(value) + 1; // includes null terminator
                int numLines = numChars % 4 == 0 ? (numChars / 4) : (numChars / 4) + 1;
                int endPaddingCount = (numLines * 32) - (numChars * 8);

                //Calculate binary chunk resizing
                newSymbol->BinValue = calloc(((numLines * 32) + numLines + 1), sizeof(char));
                if (!newSymbol->BinValue)
                {
                    return;
                }

                // Write binary representation of characters to binary chunk
                for (int i = 0; i < numChars; i++)
                {
                    linePosition = (linePosition + 1) % 4;
                    char localBinary[9] = "0";

                    charToBin(localBinary, value[i]);

                    strcat(newSymbol->BinValue, localBinary);

                    if (linePosition == 0)
                    {
                        strcat(newSymbol->BinValue, "\n");
                    }
                }

                // Write end padding for last line of characters
                if (pad)
                {
                    linePosition = 1;
                    for (int j = 0; j < endPaddingCount; j++)
                    {
                        strcat(newSymbol->BinValue, "0");
                    }
                    if (endPaddingCount > 0)
                    {
                        strcat(newSymbol->BinValue, "\n");
                    }
                }

                addSymbol(symbolTable, newSymbol); // ADD SYMBOL TO TABLE
                *dataAddress += numLines * 4;
            }
            else if (strcmp(type, ".word") == 0)
            { // Parse Word type

                linePosition = 0;
                if (strchr(value, ':'))
                { // Value has some multiple

                    newSymbol->Type = HOMO_ARRAY;

                    // Scan values into number, multiple
                    int number;
                    int multiple;
                    sscanf(value, "%d:%d", &number, &multiple);

                    //Set BinValue to proper size (# 32 char lines + newLines + nullTerminator)
                    newSymbol->BinValue = calloc(((multiple * 32) + multiple + 1), sizeof(char));
                    if (!newSymbol->BinValue)
                    {
                        return;
                    }

                    //Get binary string from number
                    char localBinary[33];
                    localBinary[32] = '\0';
                    decToBin(localBinary, number, "label");

                    //Create binary printable chunk for this array
                    for (int i = 0; i < multiple; i++)
                    {
                        strcat(newSymbol->BinValue, localBinary);
                        strcat(newSymbol->BinValue, "\n");

                        *dataAddress += 4;
                    }

                    addSymbol(symbolTable, newSymbol); // ADD SYMBOL TO TABLE
                }
                else if (strchr(sanitizedLine, ','))
                { // Value is some array of values

                    newSymbol->Type = HETERO_ARRAY;

                    int arrayCounter = 0;
                    // Add all 32 bit binary strings to binary chunk
                    newSymbol->BinValue = calloc(1, sizeof(char));
                    while (value)
                    {
                        arrayCounter++;
                        //Set BinValue to proper size (# 32 char lines + newLines + nullTerminator)
                        newSymbol->BinValue = realloc(newSymbol->BinValue, ((arrayCounter * 32) + arrayCounter + 1) * sizeof(char));
                        if (!newSymbol->BinValue)
                        {
                            return;
                        }
                        int number;
                        sscanf(value, "%d", &number);

                        //Get binary string from number
                        char localBinary[33];
                        localBinary[32] = '\0';
                        decToBin(localBinary, number, "label");

                        strcat(newSymbol->BinValue, localBinary);
                        strcat(newSymbol->BinValue, "\n");

                        *dataAddress += 4;

                        value = strtok(NULL, ", \t");
                    }
                    addSymbol(symbolTable, newSymbol); // ADD SYMBOL TO TABLE
                }
                else
                { // Value is not an array of any kind

                    newSymbol->Type = NO_ARRAY;

                    // Scan values into number, multiple
                    int number;
                    sscanf(value, "%d", &number);

                    //Get binary string from number
                    char localBinary[33];
                    localBinary[32] = '\0';
                    decToBin(localBinary, number, "label");

                    //Set BinValue to proper size (# 32 char lines + newLines + nullTerminator)
                    newSymbol->BinValue = calloc((32 + 1 + 1), sizeof(char));
                    if (!newSymbol->BinValue)
                    {
                        return;
                    }
                    strcat(newSymbol->BinValue, localBinary);
                    strcat(newSymbol->BinValue, "\n");

                    addSymbol(symbolTable, newSymbol); // ADD SYMBOL TO TABLE
                    *dataAddress += 4;
                }
            }

            free(name);
            free(type);
            free(value);
        }
        else if (inText)
        { // IN TEXT SECTION

            if (strchr(sanitizedLine, ':'))
            {
                char *name = calloc(strlen(sanitizedLine), sizeof(char));
                strncpy(name, sanitizedLine, strlen(sanitizedLine) - 1);
                name[strlen(name)] = '\0'; //null terminate name

                //Initialize Symbol
                Symbol *newSymbol = initSymbol();
                newSymbol->Name = calloc(strlen(name) + 1, sizeof(char));
                strcpy(newSymbol->Name, name);
                newSymbol->Address = *textAddress;
                newSymbol->Type = TEXT_LABEL;

                //Add symbol to table
                addSymbol(symbolTable, newSymbol);
                free(name);
            }
            else
            {
                *textAddress += 4;

                //BLT represented as 2 instructions, therefore increment once more
                if(strstr(sanitizedLine, "blt")) {
                    *textAddress += 4;
                }
            }
        }
    }
}

/**
 * Prints the data segment of a SymbolTable
 */
void printDataSegment(FILE *writeFile, SymbolTable *symbolTable)
{

    fprintf(writeFile, "%s", "\n");
    for (int i = 0; i < symbolTable->Size; i++)
    {
        if (symbolTable->Table[i]->Type != TEXT_LABEL)
        {
            fprintf(writeFile, "%s", symbolTable->Table[i]->BinValue);
        }
    }
}

/**
 * Prints all symbols in a SymbolTable
 */
void printSymbols(SymbolTable *symbolTable, FILE *writeFile)
{
    for (int i = 0; i < symbolTable->Size; i++)
    {
        fprintf(writeFile, "0x%08X %s\n", symbolTable->Table[i]->Address, symbolTable->Table[i]->Name);
    }
}

/**
 * Sanitizes an input string by trimming undesired characters
 */
char *getSanitizedLine(char* sanitizedLine, char *line)
{
    // Sanitize line by trimming whitespaces and removing special characters
    line[strcspn(line, "\n")] = 0;

    //If line contains comments, produce clean substring
    if (strchr(line, '#'))
    {
        line[strchr(line, '#') - line] = '\0'; // cut out comment
    }

    //trim whiteSpace
    trimLeadingTrailingWhitespace(sanitizedLine, line);
    return sanitizedLine;
}
