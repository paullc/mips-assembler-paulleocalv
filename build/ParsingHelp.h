#ifndef PARSINGHELP_H
#define PARSINGHELP_H

#include "ParseResult.h"
#include "MnemonicTable.h"

/**
 * Parses R Type instructions
 */
void parseRType(const char *const pASM, ParseResult *result, Instruction *instruction, SymbolTable *symbolTable, FILE *writeFile, int *instrAddress);

/**
 * Parses I Type instructions
 */
void parseIType(const char *const pASM, ParseResult *result, Instruction *instruction, SymbolTable *symbolTable, FILE *writeFile, int *instrAddress);

/**
 * Parses J Type instructions
 */
void parseJType(const char *const pASM, ParseResult *result, Instruction *instruction, SymbolTable *symbolTable, FILE *writeFile, int *instrAddress);

/**
 * Prints R Type instructions
 */
void printRType(ParseResult *result, Instruction *instruction, FILE *writeFile);

/**
 * Prints I Type instructions
 */
void printIType(ParseResult *result, Instruction *instruction, FILE *writeFile);

/**
 * Prints J Type instructions
 */
void printJType(ParseResult *result, Instruction *instruction, FILE *writeFile);

/**
 *  Converts an integer to its binary representation string.
 */
void decToBin(char *binImm, int imm, char *type);

/**
 * Converts an character to its binary representation string.
 */
void charToBin(char *localBinary, char character);

/**
 * Trims whitespace from a string
 */
char *trimLeadingTrailingWhitespace(char *sanitizedLine, char *str);

/**
 * Initializes the fields of a ParseResult object
 */
void initParseResult(ParseResult *result);

#endif